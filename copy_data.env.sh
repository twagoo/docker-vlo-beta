VLO_VERSION="4.9.3"
REMOTE_RELEASE_URL="https://github.com/clarin-eric/VLO/releases/download/${VLO_VERSION}/vlo-${VLO_VERSION}-docker.tar.gz"
NAME="vlo-${VLO_VERSION}-docker"

